section .text

global exit
global string_length
global print_string
global print_err
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy

%define EXIT_CALL 60
%define WRITE_CALL 1
%define READ_CALL 0

%define STDOUT 1
%define STDERR 2
%define STDIN 0

%define NEWLINE 0xA

%macro read_char_macro 2
    call read_char
    cmp al, 0
    je %2
    cmp al, 0x20
    je %1
    cmp al, 0x9
    je %1
    cmp al, 0xA
    je %1
%endmacro
 
; Принимает код возврата и завершает текущий процесс
exit:
    mov rax, EXIT_CALL
    syscall
    ret

; Принимает указатель на нуль-терминированную строку, возвращает её длину
; автомат: читать символы, пока не ноль добавлять 1 в rax -> если ноль, выход
; rax - result
string_length:
    .start:
        xor rax, rax
    .count:
        cmp byte [rdi+rax], 0
        je .end
        inc rax
        jmp .count
    .end:
        ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
; rdi - string
print_string:
    xor rax, rax
    call string_length
    mov rdx, rax    ; rdx has size count (line length)
    mov rax, WRITE_CALL      ; rax has syscall num for write
    mov rsi, rdi    ; rsi gets your string line from rdi as it is char buf for write
    mov rdi, STDOUT      ; descriptor (1 - stdout)
    syscall           ; вызов write
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stderr
; rdi - string

print_err:
	xor rax, rax
	call string_length
	mov rdx, rax
	mov rax, WRITE_CALL
	mov rsi, rdi
	mov rdi, STDERR
	syscall
	ret

; Принимает код символа и выводит его в stdout
; Взять адрес на символ и отдать через rdi. Идея: стек
; rdi - char, то есть сам символ
print_char:
    xor rax, rax
    push rdi
    mov rdi, rsp
    call print_string
    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
; Решение: положить строчку в rdi и сделать print_char
print_newline:
    xor rax, rax
    mov rdi, NEWLINE
    call print_char
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
; rdi - беззнаковое 8-байтовое число в десятичном формате
; Решение: 1) форматирование путём деления на 10 и перевода в ASCII, 2) call print_string || print_char
; Done: less invokations -> better algo
print_uint:
    .start:
        xor rax, rax
        mov rsi, rsp
        sub rsp, 24
        mov r8, 10
        mov rax, rdi
        mov byte[rsi], 0; чтобы отловить конец
    .format:
        xor rdx, rdx
        div r8
        dec rsi
        add dl, '0'
        mov byte[rsi], dl
        cmp rax, 0
        je .print
        jmp .format
    .print:
        mov rdi, rsi
        call print_string
        add rsp, 24
    .end:
        ret

; Выводит знаковое 8-байтовое число в десятичном формате
; rdi - снова приходит сюда
; Решение: меньше нуля -> печать минуса, потом остального или сразу печать uint
print_int:
    .start:
        xor rax, rax
        cmp rdi, 0
        jns .interpret_as_uint; если положительное, сразу печать
    .neg:
        push rdi
        mov rdi, '-'
        call print_char
        pop rdi
        neg rdi
    .interpret_as_uint:
        push rdi
        call print_uint
        pop rdi
    ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
; rdi - первая строка
; rsi - вторая строка
; rax - результат
; Решение: конечный автомат с циклом сравнения, error->выход с 0, дошли до конца->1
string_equals:
    .start:
        xor rax, rax
    .compare_len:
        push rdi
        call string_length
        mov r8, rax
        mov rdi, rsi
        call string_length
        pop rdi ; оказывается, если это делать чуть ниже, то всё ломается :(
        cmp rax, r8
        jne .error
    .compare:
        mov al, byte [rdi] ; текущие символы
        cmp al, byte [rsi]
        jne .error
        inc rdi
        inc rsi
        cmp r8, 0 ; в r8 выше сохранили string_length
        je .success
        dec r8
        jmp .compare
    .success:
        mov rax, 1
        ret
    .error:
        xor rax, rax
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    .start:
        push 0
        xor rax, rax
    .call:
        mov rdx, 1
        mov rax, READ_CALL
        mov rsi, rsp
        mov rdi, STDIN
        syscall
    .end:
        pop rax
        ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
; rdi - адрес начала буфера
; rsi - размер буфера
; rdx - (результат) длина слова
; rax - (результат) адрес буфера
; Решение: конечный автомат
; skip - пропуск лишнего, read - цикл чтения, success и error.
; check read_char_macro
read_word:
    .start:
        xor rax, rax
        xor rdx, rdx
        push rdi
        mov r8, rdi
    .skip:
        read_char_macro .skip, .error
    .read:
        dec rsi
        jz .error
        mov byte[r8], al
        inc r8
        read_char_macro .success, .success
        jmp .read
    .success:
        mov byte[r8], 0
        pop rax; результат - адрес буфера
        mov rdx, r8
        sub rdx, rax
        ret
    .error:
        pop rdi; достали из стека обратно :|
        xor rax, rax
        xor rdx, rdx
        ret

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
; rdi - строка
; Алгоритм: читаем что-то, вычитаем '0', получаем циферку, сохраняем; иначе -> число прочитать не удалось :|
parse_uint:
    .start:
        xor rax, rax
        xor rdx, rdx
        push rsi; чтобы спокойно использовать sil
        xor rsi, rsi
        xor r9, r9
        mov r8, 10
    .parse:
        xor rsi, rsi
        mov sil, byte[rdi]
        sub sil, '0'
        cmp sil, 0
        jb .end
        cmp sil, 9
        ja .end
        mul r8
        add rax, rsi
        inc rdi
        inc r9
        jmp .parse
    .end:
        mov rdx, r9; прикол в том, что напрямую с rdx не работало :|
        pop rsi
        ret




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
; rdi - строка
; rax - (результат) число
; rdx - его длина, включая знак
parse_int:
    .start:
        xor rax, rax
        xor rsi, rsi
        mov sil, byte [rdi]
        cmp sil, '-'
        je .make_neg
        call parse_uint
        jmp .end
    .make_neg:
        inc rdi
        call parse_uint
        neg rax
        inc rdx
        cmp rax, 0; проверка, что что-то прочиталось
        jne .end
        xor rdx, rdx
    .end:
        ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
; rdi - строка
; rsi - буфер
; rdx - длина буфера
; rax - (результат) длина строки
string_copy:
    .start:
        xor r8, r8
        call string_length
        inc rax ; this is fine (meme)
        mov r8, rax ; this is fine (meme)
        cmp rax, rdx
        ja .error
    .main:
        cmp r8, 0
        je .end
        mov r10b, byte[rdi]
        mov byte[rsi], r10b
        inc rsi
        inc rdi
        dec r8
        jmp .main
    .end:
        ret
    .error:
        xor rax, rax
        jmp .end

ASM        = nasm
FLAGS      = -felf64 -g
LD         = ld -o

.PHONY: clean

build: main

lib.o: lib.asm
	$(ASM) $(FLAGS) -o $@ $<

dict.o: dict.asm
	$(ASM) $(FLAGS) -o $@ $<

main.o: main.asm lib.inc colon.inc words.inc
	$(ASM) $(FLAGS) -o $@ $<

main: main.o lib.o dict.o
	$(LD) $@ $^

clean:
	rm -rf *.o
	rm -rf main

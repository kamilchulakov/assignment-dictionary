; создать функцию find_word. Она принимает два аргумента:
; Указатель на нуль-терминированную строку.
; Указатель на начало словаря.

; find_word пройдёт по всему словарю в поисках подходящего ключа.
; Если подходящее вхождение найдено, вернёт адрес начала вхождения в   словарь (не значения), иначе вернёт 0.
%include "lib.inc"

%ifndef
%define LABEL_SKIP 8
%endif

global find_word

; rdi - Указатель на нуль-терминированную строку.
; rsi - Указатель на начало словаря.'
; rax - (Результат) адрес начала вхождения или 0
find_word:
	.start:
		xor rax, rax
		test rsi, rsi
		jz .error
	.loop:
		push rdi; caller-saved
		push rsi
		add rsi, LABEL_SKIP
		call string_equals
        pop rsi
        pop rdi
        cmp rax, 1
        je .success
        mov rsi, [rsi] ; next pointer
        test rsi, rsi
        jz .error
        jmp .loop
	.success:
		mov rax, rsi
		jmp .end
	.error:
		xor rax, rax
	.end:
		ret



%define MAX_CAPACITY 256 ; 255+null

section .text
%include "lib.inc"
%include "colon.inc"
%include "words.inc"

section .data
input_error: db "Invalid input",0
invalid_key: db "No such key", 0
vars: db "Type first, second or author.", 0

%ifndef
%define LABEL_SKIP 8
%endif


section .text
global _start
extern find_word

_start:
    .start:
        xor rax, rax
        sub rsp, MAX_CAPACITY
        mov rdi, vars
        call print_string
        call print_newline
        mov rdi, rsp
	    mov rsi, MAX_CAPACITY
	    call read_word
	    test rax, rax
	    jz .input_err
	    jmp .input_ok
    .input_err:
        mov rdi, input_error
        call print_err
        call print_newline
        mov rdi, 1
        jmp .end
    .input_ok:
        mov rdi, rax
	    mov rsi, NXT
	    call find_word
        test rax, rax
        jz .no_key
        add rax, LABEL_SKIP ; адрес
        add rax, rdx ; длина ключа
        inc rax; нуль-терминатор
        mov rdi, rax
        call print_string
        call print_newline
        xor rdi, rdi
        jmp .end
    .no_key:
        mov rdi, invalid_key
	    call print_err
	    call print_newline
	    mov rdi, 1
    .end:
        add rsp, MAX_CAPACITY
        call exit

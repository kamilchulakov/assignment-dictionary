; Создайте макрос colon с двумя аргументами: ключом и меткой, которая будет сопоставлена значению.
; Эта метка не может быть сгенерирована из самого значения, так как в строчке могут быть символы,
; которые не могут встречаться в метках, например, арифметические знаки, знаки пунктуации и т.д.
; После использования такого макроса можно напрямую указать значение, сопоставляемое ключу.

; colon "third word", third_word
; db "third word explanation", 0 - null-terminated?

; x1:
;   dq x2
;   dq 100

%macro key_colon 1
    %ifstr %1
		db %1, 0
	%else
		%fatal "Key must be string!"
	%endif
%endmacro

%define NXT 0
%macro colon 2
	%ifid %2
	    %%NEXT:
	        dq NXT;
	        key_colon %1
	    %2:
            %define NXT %%NEXT ; is it possible to use assign? no, due to words.inc: " non-constant value given to `%assign'"
	%else
	    %fatal "Label must be ID!"
    %endif
%endmacro